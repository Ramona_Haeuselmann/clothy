package com.geronimo.clothy.util;

public class InUseException extends Exception {
    public InUseException(String message){
        super(message);
    }
}
