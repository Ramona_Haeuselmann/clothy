package com.geronimo.clothy.util.displayStrings;

import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.geronimo.clothy.R;
import androidx.recyclerview.widget.RecyclerView;

// Provide a reference to the views for each data item
// Complex data items may need more than one view per item, and
// you provide access to all the views for a data item in a view holder
public class SingleStringViewHolder extends RecyclerView.ViewHolder {
    public RelativeLayout ViewForeground;
    protected TextView lineContent;

    public SingleStringViewHolder(View v) {
        super(v);

        ViewForeground = v.findViewById(R.id.view_foreground);
        lineContent = v.findViewById(R.id.line_content);
    }
    public final void bind(String item) {
        lineContent.setText(item);
    }
}