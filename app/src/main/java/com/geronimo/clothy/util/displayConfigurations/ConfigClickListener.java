package com.geronimo.clothy.util.displayConfigurations;

import android.view.View;

public interface ConfigClickListener {
    void onItemClick(View v, int position);

//    public void onItemClick(View v, FrozenItem clickedItem);
}
