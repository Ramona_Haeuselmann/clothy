package com.geronimo.clothy.util;


import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by Ramona on 22.03.2018.
 */

public abstract class MyRecyclerScroll extends RecyclerView.OnScrollListener {
    private static final float MINIMUM = 25;

    private int _scrollDist = 0;
    private boolean _isVisible = true;

    @Override
    public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);

        //dy:   scrolling down -> positive
        //      scrolling up -> negative

        if (_isVisible && _scrollDist > MINIMUM) {
            hide();
            _scrollDist = 0;
            _isVisible = false;
        }
        else if (!_isVisible && _scrollDist < -MINIMUM) {
            show();
            _scrollDist = 0;
            _isVisible = true;
        }

        if ((_isVisible && dy > 0) || (!_isVisible && dy < 0)) {
            _scrollDist += dy;
        }

    }

    public abstract void show();
    public abstract void hide();

}
