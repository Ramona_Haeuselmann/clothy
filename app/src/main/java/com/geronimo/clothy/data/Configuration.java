package com.geronimo.clothy.data;

import java.util.Objects;

public class Configuration {
    private long id;
    private String name;
    private int temperature;
    private Detergent detergent;



    //needed for serialization
    public Configuration(){}

    public Configuration(long id, String name, int temperature, Detergent detergent){
        this.id = id;
        this.name = name;
        this.temperature = temperature;
        this.detergent = detergent;
    }

    public String getName(){
        return this.name;
    }

    public Detergent getDetergent(){
        return this.detergent;
    }

    public int getTemperature() {
        return this.temperature;
    }

    public String getDetails(){
        return getDetergent().getName() + " | " + getTemperature() + "°C";
    }

    public long getId(){
        return this.id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Configuration that = (Configuration) o;
        return id == that.id &&
                temperature == that.temperature &&
                Objects.equals(name, that.name) &&
                Objects.equals(detergent, that.detergent);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, temperature, detergent);
    }
}
