package com.geronimo.clothy.data;

public enum ConfigOrder {
    NAME_ASC,
    NAME_DESC,
    TEMP_ASC,
    TEMP_DESC,
    DETERGENT_ASC,
    DETERGENT_DESC
}
