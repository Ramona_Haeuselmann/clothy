package com.geronimo.clothy.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.Toast;

import com.geronimo.clothy.AppData;
import com.geronimo.clothy.R;
import com.geronimo.clothy.util.InUseException;
import com.geronimo.clothy.util.MyRecyclerScroll;
import com.geronimo.clothy.util.displayStrings.SingleStringAdapter;
import com.geronimo.clothy.util.displayStrings.SingleStringSwipeHelper;
import com.geronimo.clothy.util.displayStrings.SingleStringViewHolder;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class FragTemperatures extends Fragment
        implements SingleStringSwipeHelper.SingleStringSwipeHelperListener {
    private static SingleStringAdapter tempAdapter;
    private RecyclerView recyclerView;
    private View fragmentView;
    private FloatingActionButton fab;
    private SingleStringSwipeHelper swipeHelper;

    private static final int GET_TEMP_REQUEST_CODE = 1;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        //restore(savedInstanceState);

        // Inflate the layout for this fragment
        this.fragmentView = inflater.inflate(R.layout.frag_string_list, container, false);
        this.recyclerView = this.fragmentView.findViewById(R.id.listTwoLines);
        this.fab = this.fragmentView.findViewById(R.id.fabAdd);

        setHasOptionsMenu(false);

        setUpFabBehaviour();

        setUpList();

        return this.fragmentView;
    }

    private void setUpList(){
        // use a linear layout manager
        this.recyclerView.setLayoutManager(
                new LinearLayoutManager(getContext())
        );
        //this.recyclerView.setItemAnimator(new DefaultItemAnimator());

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        this.recyclerView.setHasFixedSize(true);

        //divider
        this.recyclerView.addItemDecoration(
                new DividerItemDecoration(recyclerView.getContext(), LinearLayoutManager.VERTICAL)
        );

        //adapter
        setTempAdapter(new SingleStringAdapter(
                AppData.getManager().getTemperaturesWithUnit()
        ));

        this.recyclerView.setAdapter(tempAdapter);


        //register swipes
        this.swipeHelper = new SingleStringSwipeHelper(0, ItemTouchHelper.LEFT, this);
        new ItemTouchHelper(this.swipeHelper).attachToRecyclerView(this.recyclerView);
        enableItemSwipe(true);
    }

    private void setTempAdapter(SingleStringAdapter adapter){
        tempAdapter = adapter;
        AppData.getManager().setTempAdapter(tempAdapter);
    }

    private void setUpFabBehaviour(){
        //hide upon scrolling down and reappear when scrolled up (known as the Quick Return Pattern)
        final int fabMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 16, getResources().getDisplayMetrics());
        recyclerView.addOnScrollListener(new MyRecyclerScroll() {
            @Override
            public void show() {
                fab.animate().translationY(0).setInterpolator(new DecelerateInterpolator(2)).start();
            }

            @Override
            public void hide() {
                fab.animate().translationY(fab.getHeight() + fabMargin).setInterpolator(new AccelerateInterpolator(2)).start();
            }
        });

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fabClick();
            }
        });
    }

    private void fabClick(){
        Bundle bundle = new Bundle();
        bundle.putSerializable("Type", MyDialog.MyDialogType.ADD_TEMP);
        FragmentManager fragmentManager = getFragmentManager();
        MyDialog d = new MyDialog();
        d.setArguments(bundle);
        d.setTargetFragment(FragTemperatures.this, GET_TEMP_REQUEST_CODE);
        d.show(fragmentManager, "addTemp");
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if( resultCode != MyDialog.RESULT_OK ) {
            return;
        }
        if( requestCode == GET_TEMP_REQUEST_CODE) {
            int temp = data.getIntExtra("Temperature", Integer.MIN_VALUE );
            if (temp == Integer.MIN_VALUE){
                Toast.makeText(getContext(),getString(R.string.internal_error), Toast.LENGTH_SHORT).show();
            } else if (AppData.getManager().tempExists(temp)){
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("Type", MyDialog.MyDialogType.CANT_ADD_TEMP);
                    FragmentManager fragmentManager = getChildFragmentManager();
                    MyDialog d = new MyDialog();
                    d.setArguments(bundle);
                    d.show(fragmentManager, "cantAddTemp");
            } else{
                //add
                AppData.getManager().addTemp(temp);
            }
        }
    }


//    private void fabClick(){
//        Bundle bundle = new Bundle();
//        bundle.putSerializable("Type", MyDialog.MyDialogType.ADD_TEMP);
//        FragmentManager fragmentManager = getChildFragmentManager();
//        MyDialog d = new MyDialog();
//        d.setArguments(bundle);
//        d.show(fragmentManager, "addTemp");
//
////        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
////        builder.setTitle(getString(R.string.add_temp_title));
////
////        // Set up the input
////        LayoutInflater linf = LayoutInflater.from(getContext());
////        final View inflator = linf.inflate(R.layout.temp_input, null);
////
////        //final EditText input = new EditText(getContext());
////        // Specify the type of input expected
////        //input.setInputType(InputType.TYPE_CLASS_NUMBER);
////        //builder.setView(input);
////
////        builder.setView(inflator);
////        final EditText input = (EditText) inflator.findViewById(R.id.tempInput);
////
////        // Set up the buttons
////        builder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
////            @Override
////            public void onClick(DialogInterface dialog, int which) {
////                int newTemp = Integer.parseInt(input.getText().toString());
////                if (AppData.getManager().tempExists(newTemp)){
////                    Bundle bundle = new Bundle();
////                    bundle.putSerializable("Type", MyDialog.MyDialogType.CANT_ADD_TEMP);
////                    FragmentManager fragmentManager = getChildFragmentManager();
////                    MyDialog d = new MyDialog();
////                    d.setArguments(bundle);
////                    d.show(fragmentManager, "cantAddTemp");
//////                    AlertDialog.Builder dlgAlert  = new AlertDialog.Builder(getContext());
//////                    String msg = getString(R.string.temp_exists) + System.lineSeparator()
//////                            + getString(R.string.add_not_possible);
//////                    dlgAlert.setMessage(msg);
//////                    dlgAlert.setTitle(getString(R.string.attention));
//////                    dlgAlert.setPositiveButton(getString(R.string.ok), null);
//////                    dlgAlert.setCancelable(false);
//////                    dlgAlert.setIcon(R.drawable.ic_warning);
//////                    dlgAlert.create().show();
////                } else{
////                    //add
////                    AppData.getManager().addTemp(newTemp);
////                }
////            }
////        });
////        builder.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
////            @Override
////            public void onClick(DialogInterface dialog, int which) {
////                dialog.cancel();
////            }
////        });
////        builder.show();
//
//
//    }

    /**
     * callback when recycler view is swiped
     * item will be removed on swiped
     * undo option will be provided in snackbar to restore the item
     */
    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction, int position) {
        if (viewHolder instanceof SingleStringViewHolder) {
            int adapterPos = viewHolder.getAdapterPosition();
            // backup of removed item for undo purpose
            final int deletedIndex = viewHolder.getAdapterPosition();
            final int deletedItem = AppData.getManager().getTemperatures().get(deletedIndex);

            // remove the item
            try {
                AppData.getManager().removeTemp(adapterPos);
            } catch (InUseException e){
                FragmentManager fragmentManager = getChildFragmentManager();
                MyDialog dialog = new MyDialog();
                Bundle bundle = new Bundle();
                bundle.putSerializable("Type", MyDialog.MyDialogType.CANT_DELETE_TEMP);
                dialog.setArguments(bundle);
                dialog.show(fragmentManager, "cantDeleteTemp");
//                AlertDialog.Builder dlgAlert  = new AlertDialog.Builder(getContext());
//                String msg = getString(R.string.temp_in_use) + System.lineSeparator()
//                        + getString(R.string.remove_not_possible);
//                dlgAlert.setMessage(msg);
//                dlgAlert.setTitle(getString(R.string.attention));
//                dlgAlert.setPositiveButton(getString(R.string.ok), null);
//                dlgAlert.setCancelable(false);
//                dlgAlert.setIcon(R.drawable.ic_warning);
//                dlgAlert.create().show();
                tempAdapter.notifyDataSetChanged();
                return;
            }
            //_recyclerViewAdapter.notifyItemRemoved(deletedIndex);

            // showing snack bar with Undo option
            Snackbar snackbar = Snackbar
                    .make(fragmentView, getResources().getString(R.string.item_removed), Snackbar.LENGTH_LONG);
            snackbar.setAction(getResources().getString(R.string.undo), new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // undo is selected, restore the deleted item
                    // _recyclerViewAdapter.notifyItemInserted(AppData.getFreezerManager().addFrozenItem(deletedItem));
                    AppData.getManager().addTemp(deletedItem);
                }
            });
            snackbar.show();
        }
    }

    private void enableItemSwipe(boolean enable){
        if (this.swipeHelper != null) {
            this.swipeHelper.setSwipeEnabled(enable);
        }
    }
}
