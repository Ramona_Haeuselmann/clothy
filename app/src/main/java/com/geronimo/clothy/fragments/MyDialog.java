package com.geronimo.clothy.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.geronimo.clothy.AppData;
import com.geronimo.clothy.R;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;

public class MyDialog extends DialogFragment {
    public enum MyDialogType{
        CANT_DELETE_TEMP,
        CANT_DELETE_DET,
        CANT_ADD_TEMP,
        CANT_ADD_DET,
        ADD_TEMP,
        ADD_DET,
        NONE
    }


    public static final int RESULT_OK = 0;
    public static final int RESULT_CANCEL = 1;



    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        MyDialogType type = MyDialogType.NONE;
        Bundle bundle = getArguments();
        if (bundle != null) {
            type = (MyDialogType) bundle.getSerializable("Type");
        }

        if (type == null){
            return ups();
        }

        switch (type){
            case CANT_DELETE_TEMP:
                return createCantDeleteTemp();
            case CANT_ADD_TEMP:
                return createCantAddTemp();
            case ADD_TEMP:
                return createAddTemp();
            case ADD_DET:
                return createAddDetergent();
            case CANT_ADD_DET:
                return createCantAddDet();
            case CANT_DELETE_DET:
                return createCantDeleteDet();
            default:
                return ups();
        }
    }

    private Dialog ups(){
        AlertDialog.Builder dlgAlert  = new AlertDialog.Builder(getContext());
        dlgAlert.setMessage("Something bad happened.");
        dlgAlert.setTitle("Oops");
        dlgAlert.setPositiveButton(getString(R.string.ok), null);
        dlgAlert.setCancelable(false);
        return dlgAlert.create();
    }

    private Dialog createAddTemp(){
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle(getString(R.string.add_temp_title));
        builder.setCancelable(true);
        // Set up the input
        LayoutInflater linf = LayoutInflater.from(getContext());
        final View inflator = linf.inflate(R.layout.temp_input, null);
        builder.setView(inflator);
        final EditText input = (EditText) inflator.findViewById(R.id.tempInput);
        builder.setPositiveButton(getString(R.string.ok), null);
        // Set up the buttons
//        builder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                int temp;
//                try {
//                    temp = Integer.parseInt(input.getText().toString());
//                } catch (NumberFormatException e){
//                    temp = Integer.MIN_VALUE;
//                }
//                sendTemperature(temp);
//            }
//        });
        builder.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        final Dialog d = builder.create();

        d.setOnShowListener(new DialogInterface.OnShowListener() {

            @Override
            public void onShow(DialogInterface dialogInterface) {

                Button button = ((AlertDialog) d).getButton(AlertDialog.BUTTON_POSITIVE);
                button.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View view) {
                        int temp;
                        try {
                            temp = Integer.parseInt(input.getText().toString());
                            sendTemperature(temp);
                            //Dismiss once everything is OK.
                            d.dismiss();
                        } catch (NumberFormatException e){
                            temp = Integer.MIN_VALUE;
                        }
                    }
                });
            }
        });
        return d;
    }

    private Dialog createCantAddTemp(){
        AlertDialog.Builder dlgAlert  = new AlertDialog.Builder(getContext());
        String msg = getString(R.string.temp_exists) + System.lineSeparator()
                + getString(R.string.add_not_possible);
        dlgAlert.setMessage(msg);
        dlgAlert.setTitle(getString(R.string.attention));
        dlgAlert.setPositiveButton(getString(R.string.ok), null);
        dlgAlert.setCancelable(false);
        dlgAlert.setIcon(R.drawable.ic_warning);
        return dlgAlert.create();
    }

    private Dialog createCantDeleteTemp(){
        AlertDialog.Builder dlgAlert  = new AlertDialog.Builder(getContext());
        String msg = getString(R.string.temp_in_use) + System.lineSeparator()
                + getString(R.string.remove_not_possible);
        dlgAlert.setMessage(msg);
        dlgAlert.setTitle(getString(R.string.attention));
        dlgAlert.setPositiveButton(getString(R.string.ok), null);
        dlgAlert.setCancelable(false);
        dlgAlert.setIcon(R.drawable.ic_warning);
        return dlgAlert.create();
    }

    private void sendTemperature(int temp) {
        if( getTargetFragment() == null ) {
            return;
        }
        Intent intent = new Intent();
        intent.putExtra("Temperature", temp);

        getTargetFragment().onActivityResult(getTargetRequestCode(), RESULT_OK, intent);
        //dismiss();
    }

    private Dialog createAddDetergent(){
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle(getString(R.string.add_det_title));
        builder.setCancelable(true);
        // Set up the input
        LayoutInflater linf = LayoutInflater.from(getContext());
        final View inflator = linf.inflate(R.layout.det_input, null);
        builder.setView(inflator);
        final EditText detInput = (EditText) inflator.findViewById(R.id.detInput);
        final EditText priorityInput = (EditText) inflator.findViewById(R.id.priorityInput);

        // Set up the buttons
        builder.setPositiveButton(getString(R.string.ok), null);
        builder.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

//        builder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                sendDetergent(input.getText().toString());
//            }
//        });

        final Dialog d = builder.create();

        d.setOnShowListener(new DialogInterface.OnShowListener() {

            @Override
            public void onShow(DialogInterface dialogInterface) {

                Button button = ((AlertDialog) d).getButton(AlertDialog.BUTTON_POSITIVE);
                button.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View view) {
                        String detName = detInput.getText().toString();
                        int priority = Integer.MIN_VALUE;
                        try {
                            priority = Integer.parseInt(priorityInput.getText().toString());
                            sendDetergent(detName, priority);
                            //Dismiss once everything is OK.
                            d.dismiss();
                        } catch (NumberFormatException e){
                            priority = Integer.MIN_VALUE;
                        }
                    }
                });
            }
        });
        return d;
    }

    private Dialog createCantAddDet(){
        AlertDialog.Builder dlgAlert  = new AlertDialog.Builder(getContext());
        String msg = getString(R.string.det_exists) + System.lineSeparator()
                + getString(R.string.add_not_possible);
        dlgAlert.setMessage(msg);
        dlgAlert.setTitle(getString(R.string.attention));
        dlgAlert.setPositiveButton(getString(R.string.ok), null);
        dlgAlert.setCancelable(false);
        dlgAlert.setIcon(R.drawable.ic_warning);
        return dlgAlert.create();
    }

    private Dialog createCantDeleteDet(){
        AlertDialog.Builder dlgAlert  = new AlertDialog.Builder(getContext());
        String msg = getString(R.string.det_in_use) + System.lineSeparator()
                + getString(R.string.remove_not_possible);
        dlgAlert.setMessage(msg);
        dlgAlert.setTitle(getString(R.string.attention));
        dlgAlert.setPositiveButton(getString(R.string.ok), null);
        dlgAlert.setCancelable(false);
        dlgAlert.setIcon(R.drawable.ic_warning);
        return dlgAlert.create();
    }


    private void sendDetergent(String det, int priority) {
        if( getTargetFragment() == null ) {
            return;
        }
        Intent intent = new Intent();
        intent.putExtra("Detergent", det);
        intent.putExtra("Priority", priority);

        getTargetFragment().onActivityResult(getTargetRequestCode(), RESULT_OK, intent);
        //dismiss();
    }
}
