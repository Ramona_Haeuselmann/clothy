package com.geronimo.clothy.fragments;

import com.geronimo.clothy.util.displayConfigurations.ConfigViewHolder;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.fragment.app.FragmentManager;
import androidx.core.content.ContextCompat;


import androidx.appcompat.app.AppCompatActivity;

import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;

import com.geronimo.clothy.AppData;
import com.geronimo.clothy.R;
import com.geronimo.clothy.data.ConfigOrder;
import com.geronimo.clothy.data.Configuration;
import com.geronimo.clothy.util.displayConfigurations.ConfigAdapter;
import com.geronimo.clothy.util.displayConfigurations.ConfigSwipeHelper;
import com.geronimo.clothy.util.MyRecyclerScroll;
import com.geronimo.clothy.util.selection.MyItemLookup;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import androidx.recyclerview.selection.SelectionPredicates;
import androidx.recyclerview.selection.SelectionTracker;
import androidx.recyclerview.selection.StableIdKeyProvider;
import androidx.recyclerview.selection.StorageStrategy;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;



public class FragConfigurations extends Fragment
        implements ConfigSwipeHelper.ConfigSwipeHelperListener {

    private static ConfigAdapter configAdapter;
    private RecyclerView recyclerView;
    private View fragmentView;
    private FloatingActionButton fab;

    private SelectionTracker<Long> selectionTracker;

    private ConfigSwipeHelper swipeHelper;

    private enum State{
        CHOOSE,
        ADD
    }

    private State state = State.ADD;

    public FragConfigurations(){

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        restore(savedInstanceState);

        // Inflate the layout for this fragment
        this.fragmentView = inflater.inflate(R.layout.frag_configs, container, false);
        this.recyclerView = fragmentView.findViewById(R.id.listTwoLines);
        this.fab = fragmentView.findViewById(R.id.fabDone);

        setHasOptionsMenu(true);

        setUpFabBehaviour();

        setUpList(savedInstanceState);

        setState(this.state);

        return this.fragmentView;
    }

    private void setState(State state){
        this.state = state;

        Context context = getContext();
        if (context == null){
            return;
        }

        switch(this.state){
            case CHOOSE:
                enableItemSwipe(false);
                if (fab != null) {
                    fab.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_done));
                }
                Iterator<Long> itemIterable = selectionTracker.getSelection().iterator();
                int count = 0;
                while (itemIterable.hasNext()) {
                    count++;
                    itemIterable.next();
                }
                setToolbarColor(R.color.colorAccent);
                setTitle(Integer.toString(count));
                break;
            case ADD:
                enableItemSwipe(true);
                if (fab != null) {
                    fab.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_add));
                }
                setToolbarColor(R.color.colorPrimary);
                setTitle(R.string.nav_menu_config);
                break;
        }
    }

    private void setTitle(String title){
        AppCompatActivity activity = (AppCompatActivity)getActivity();
        if (activity == null){
            return;
        }
        ActionBar bar = activity.getSupportActionBar();
        if (bar == null){
            return;
        }
        bar.setTitle(title);
    }

    private void setTitle(int stringId){
        AppCompatActivity activity = (AppCompatActivity)getActivity();
        if (activity == null){
            return;
        }
        ActionBar bar = activity.getSupportActionBar();
        if (bar == null){
            return;
        }
        bar.setTitle(stringId);
    }

    private void setToolbarColor(int colorId){
        Context context = getContext();
        if (context == null){
            return;
        }
        AppCompatActivity activity = (AppCompatActivity)getActivity();
        if (activity == null){
            return;
        }
        ActionBar bar = activity.getSupportActionBar();
        if (bar == null){
            return;
        }
        bar.setBackgroundDrawable(
                new ColorDrawable(ContextCompat.getColor(context, colorId)));
    }

    private void restore(Bundle savedInstanceState){
        if (savedInstanceState != null) {
            this.state = (State) savedInstanceState.getSerializable("State");
        }
    }

    private void setConfigAdapter(ConfigAdapter adapter){
        configAdapter = adapter;
        AppData.getManager().setConfigAdapter(configAdapter);
    }

    private void setUpList(Bundle savedInstanceState){
        // use a linear layout manager
        this.recyclerView.setLayoutManager(
                new LinearLayoutManager(getContext())
        );
        //this.recyclerView.setItemAnimator(new DefaultItemAnimator());

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        this.recyclerView.setHasFixedSize(true);

        //divider
        this.recyclerView.addItemDecoration(
                new DividerItemDecoration(recyclerView.getContext(), LinearLayoutManager.VERTICAL)
        );

        //adapter
        setConfigAdapter(new ConfigAdapter(
                AppData.getManager().getConfigs(),
                getContext()
        ));

        this.recyclerView.setAdapter(configAdapter);

        //selection
        //set up tracker

        this.selectionTracker = new SelectionTracker.Builder<>(
                "selection-1",
                this.recyclerView,
                new StableIdKeyProvider(this.recyclerView),
                new MyItemLookup(this.recyclerView),
                StorageStrategy.createLongStorage()
        ).withSelectionPredicate(SelectionPredicates.<Long>createSelectAnything())
                .build();

        if (savedInstanceState != null){
            this.selectionTracker.onRestoreInstanceState(savedInstanceState);
        }

        configAdapter.setSelectionTracker(this.selectionTracker);

        final Activity activity = getActivity();

        if (activity != null) {
            //handle events
            this.selectionTracker.addObserver(new SelectionTracker.SelectionObserver() {
                @Override
                public void onSelectionChanged() {
                    super.onSelectionChanged();
                    activity.invalidateOptionsMenu();
                    if (selectionTracker.hasSelection()) {
                        setState(State.CHOOSE);
                    } else {
                        setState(State.ADD);
                    }
                }
            });
        }

        //register swipes
        this.swipeHelper = new ConfigSwipeHelper(0, ItemTouchHelper.LEFT, this);
        new ItemTouchHelper(this.swipeHelper).attachToRecyclerView(this.recyclerView);
        enableItemSwipe(true);
    }

    private void setUpFabBehaviour(){

        //hide upon scrolling down and reappear when scrolled up (known as the Quick Return Pattern)
        final int fabMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 16, getResources().getDisplayMetrics());
        recyclerView.addOnScrollListener(new MyRecyclerScroll() {
            @Override
            public void show() {
                fab.animate().translationY(0).setInterpolator(new DecelerateInterpolator(2)).start();
            }

            @Override
            public void hide() {
                fab.animate().translationY(fab.getHeight() + fabMargin).setInterpolator(new AccelerateInterpolator(2)).start();
            }
        });

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fabClick();
            }
        });
    }

    private void fabClick(){
        switch (this.state){
            case ADD:
                showAddItemDialog();
                break;
            case CHOOSE:
                if (getContext() == null){
                    return;
                }
                Iterator<Long> itemIterable = selectionTracker.getSelection().iterator();
                List<Long> selected = new ArrayList<>();
                while (itemIterable.hasNext()) {
                    selected.add(itemIterable.next());
                }
                AlertDialog.Builder dlgAlert  = new AlertDialog.Builder(getContext());
                dlgAlert.setMessage(AppData.getManager().evaluate(selected));
                dlgAlert.setTitle("Ergebnis");
                dlgAlert.setPositiveButton("OK", null);
                dlgAlert.setCancelable(true);
                dlgAlert.create().show();

//                Toast.makeText(getContext(),Integer.toString(count),
//                        Toast.LENGTH_SHORT).show();
                break;
        }
    }

    @Override
    public void onResume(){
        super.onResume();
        if (getActivity() != null) {
            getActivity().invalidateOptionsMenu();
        }
    }

    @Override
    public void onPause(){
        super.onPause();

    }

    @Override
    public void onStop(){
        super.onStop();
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
    }

    @Override
    public void onDetach(){
        super.onDetach();
    }
    @Override
    public void onDestroyView(){
        super.onDestroyView();
        setToolbarColor(R.color.colorPrimary);
    }

//    private void showList(View view){
//        // use this setting to improve performance if you know that changes
//        // in content do not change the layout size of the RecyclerView
//        this.recyclerView.setHasFixedSize(true);
//
//        // use a linear layout manager
//        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
//        this.recyclerView.setLayoutManager(layoutManager);
//        this.recyclerView.setItemAnimator(new DefaultItemAnimator());
//
//        //divider
//        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(), LinearLayoutManager.VERTICAL);
//        this.recyclerView.addItemDecoration(dividerItemDecoration);
//
//        // specify an adapter
//        recyclerViewAdapter = new ConfigAdapter(AppData.getManager().getConfigs());
////        recyclerViewAdapter.setOnItemClickListener(new ConfigClickListener() {
////            @Override
////            public void onItemClick(View v, int position) {
////                //itemClick(v, position);
////                //showViewItemDialog(position);
////                //Toast.makeText(getContext(),"click: "+ recyclerViewAdapter.getItem(position).getName(), Toast.LENGTH_SHORT).show();
////            }
////        });
//
//        this.recyclerView.setAdapter(recyclerViewAdapter);
//
//        //selection
//        this.selectionTracker = new SelectionTracker.Builder<>(
//                "my-selection-id",
//                this.recyclerView,
//                new MyItemKeyProvider(1, AppData.getManager().getConfigs()),
//                new MyItemLookup(this.recyclerView),
//                //StorageStrategy.createLongStorage()
//                StorageStrategy.createStringStorage()
//        )
//
//                .withOnDragInitiatedListener(new OnDragInitiatedListener() {
//                    @Override
//                    public boolean onDragInitiated(@NonNull MotionEvent e) {
//                        //TODO implement
//                        Log.i("Drag: ", "Drag");
//                        return true;
//                    }
//                }).build();
//
//
//        recyclerViewAdapter.setSelectionTracker(selectionTracker);
//
////        selectionTracker = new SelectionTracker.Builder<Long>(
////                "mySelection",
////                recyclerView,
////                new StableIdKeyProvider(recyclerView),
////                new MyItemLookup(recyclerView),
////                StorageStrategy.createLongStorage()
////            ).withSelectionPredicate(
////                SelectionPredicates.<Long>createSelectAnything()
////        ).build();
////        recyclerViewAdapter.setSelectionTracker(selectionTracker);
//
//        selectionTracker.addObserver(new SelectionTracker.SelectionObserver() {
//            @Override
//            public void onItemStateChanged(@NonNull Object key, boolean selected) {
//                super.onItemStateChanged(key, selected);
//            }
//
//            @Override
//            public void onSelectionRefresh() {
//                super.onSelectionRefresh();
//            }
//
//            @Override
//            public void onSelectionChanged() {
//                super.onSelectionChanged();
//                if (selectionTracker.hasSelection()){
//                    state = State.CHOOSE;
//                    enableItemSwipe(false);
//                    fab.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_done));
//                } else {
//                    state = State.ADD;
//                    enableItemSwipe(true);
//                    fab.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_add));
//                }
//
////                if (selectionTracker.hasSelection() && actionMode == null) {
////                    actionMode = getActivity().startActionMode(new ActionModeController(getContext(), selectionTracker));
////                    //getActivity().setMenuItemTitle(selectionTracker.getSelection().size());
////                } else if (!selectionTracker.hasSelection() && actionMode != null) {
////                    actionMode.finish();
////                    actionMode = null;
////                } else {
////                    //setMenuItemTitle(selectionTracker.getSelection().size());
////                }
////                Iterator<Configuration> itemIterable = selectionTracker.getSelection().iterator();
////                while (itemIterable.hasNext()) {
////                    //TODO implement
////
////                    Log.i("Selected: ", itemIterable.next().getName());
////                }
//            }
//
//            @Override
//            public void onSelectionRestored() {
//                super.onSelectionRestored();
//            }
//        });
//
//        //TODO restore selection
//        //Problem: access view -> how??
////        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
////        FrameLayout layout = (FrameLayout) inflater.inflate(R.layout.list_two_lines, null);
////        FrameLayout v  = (FrameLayout) layout.findViewById(R.id.layoutListTwoLines);
//
////        for (int i = 0; i < recyclerViewAdapter.getItemCount(); i++){
////            setSelectionColor(v, i);
////        }
//
//        //register swipes
//        this.swipeHelper = new ConfigSwipeHelper(0, ItemTouchHelper.LEFT, this);
//        new ItemTouchHelper(this.swipeHelper).attachToRecyclerView(recyclerView);
//        enableItemSwipe(true);
//    }

    private void enableItemSwipe(boolean enable){
        if (this.swipeHelper != null) {
            this.swipeHelper.setSwipeEnabled(enable);
        }
    }

//    private void setSelectionColor(View v, int position){
//        Configuration config = recyclerViewAdapter.getItem(position);
//
//        v.findViewById(R.id.view_foreground).setBackgroundColor(
//                config.isSelected() ? ContextCompat.getColor(getContext(), R.color.colorAccent) :
//                        ContextCompat.getColor(getContext(), R.color.defaultLightBackground));
//    }

//    private void itemClick(View v, int position){
//        Configuration config = recyclerViewAdapter.getItem(position);
//        config.setSelected(!config.isSelected());
//        setSelectionColor(v, position);
////        v.findViewById(R.id.view_foreground).setBackgroundColor(
////                config.isSelected() ? ContextCompat.getColor(getContext(), R.color.colorAccent) :
////                        ContextCompat.getColor(getContext(), R.color.defaultLightBackground));
//        if (AppData.getManager().isConfigSelected()){
//            this.state = State.CHOOSE;
//            this.fab.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_done));
//        } else {
//            this.state = State.ADD;
//            this.fab.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_add));
//        }
//    }

    /**
     * callback when recycler view is swiped
     * item will be removed on swiped
     * undo option will be provided in snackbar to restore the item
     */
    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction, int position) {
        if (viewHolder instanceof ConfigViewHolder) {
            // backup of removed item for undo purpose
            final int deletedIndex = viewHolder.getAdapterPosition();
            final Configuration deletedItem = AppData.getManager().getConfigs().get(deletedIndex);

            // remove the item
            AppData.getManager().removeConfig(viewHolder.getAdapterPosition());
            //_recyclerViewAdapter.notifyItemRemoved(deletedIndex);

            // showing snack bar with Undo option
            Snackbar snackbar = Snackbar
                    .make(fragmentView, getResources().getString(R.string.item_removed), Snackbar.LENGTH_LONG);
            snackbar.setAction(getResources().getString(R.string.undo), new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // undo is selected, restore the deleted item
                    // _recyclerViewAdapter.notifyItemInserted(AppData.getFreezerManager().addFrozenItem(deletedItem));
                    AppData.getManager().addConfig(deletedItem);
                }
            });
            snackbar.show();
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        setOptionsMenu(menu, inflater);

        super.onCreateOptionsMenu(menu, inflater);
    }

//    @Override
//    public void onPrepareOptionsMenu(Menu menu){
//        super.onPrepareOptionsMenu(menu);
//        setOptionsMenu(menu, getActivity().getMenuInflater());
//    }


    private void setOptionsMenu(Menu menu, MenuInflater inflater){
        menu.clear();
        switch(this.state){
            case CHOOSE:
                inflater.inflate(R.menu.menu_toolbar_selected, menu);
                break;
            case ADD:
                inflater.inflate(R.menu.menu_toolbar_sort, menu);
                break;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.delete:
                Iterator<Long> itemIterable = this.selectionTracker.getSelection().iterator();
                final List<Long> selected = new ArrayList<>();
                while (itemIterable.hasNext()) {
                    selected.add(itemIterable.next());
                }
                //deselect before delete
                for (Long l : selected){
                    this.selectionTracker.deselect(l);
                }
                final List<Configuration> removed = AppData.getManager().removeConfigs(selected);
                Snackbar snackbar = Snackbar
                        .make(fragmentView, getResources().getString(R.string.item_removed), Snackbar.LENGTH_LONG);
                snackbar.setAction(getResources().getString(R.string.undo), new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        // undo is selected, restore the deleted item
                        // _recyclerViewAdapter.notifyItemInserted(AppData.getFreezerManager().addFrozenItem(deletedItem));
                        AppData.getManager().addConfigs(removed);
                        for (Long l : selected){
                            selectionTracker.select(l);
                        }
                    }
                });
                snackbar.show();
                return true;
            case R.id.clearSelection:
                this.selectionTracker.clearSelection();
                return true;
            case R.id.sort:
                switch(AppData.getManager().getLastSortOrder()){
                    case NAME_ASC:
                        item.getSubMenu().findItem(R.id.menu_sort_name_asc).setChecked(true);
                        break;
                    case NAME_DESC:
                        item.getSubMenu().findItem(R.id.menu_sort_name_desc).setChecked(true);
                        break;
                    case TEMP_ASC:
                        item.getSubMenu().findItem(R.id.menu_sort_temp_asc).setChecked(true);
                        break;
                    case TEMP_DESC:
                        item.getSubMenu().findItem(R.id.menu_sort_temp_desc).setChecked(true);
                        break;
                    case DETERGENT_ASC:
                        item.getSubMenu().findItem(R.id.menu_sort_det_asc).setChecked(true);
                        break;
                    case DETERGENT_DESC:
                        item.getSubMenu().findItem(R.id.menu_sort_det_desc).setChecked(true);
                        break;
                }
                return true;

            case R.id.menu_sort_name_asc:
                AppData.getManager().sortConfigs(ConfigOrder.NAME_ASC);
                configAdapter.notifyDataSetChanged();
                return true;

            case R.id.menu_sort_name_desc:
                AppData.getManager().sortConfigs(ConfigOrder.NAME_DESC);
                configAdapter.notifyDataSetChanged();
                return true;

            case R.id.menu_sort_temp_asc:
                AppData.getManager().sortConfigs(ConfigOrder.TEMP_ASC);
                configAdapter.notifyDataSetChanged();
                return true;

            case R.id.menu_sort_temp_desc:
                AppData.getManager().sortConfigs(ConfigOrder.TEMP_DESC);
                configAdapter.notifyDataSetChanged();
                return true;

            case R.id.menu_sort_det_asc:
                AppData.getManager().sortConfigs(ConfigOrder.DETERGENT_ASC);
                configAdapter.notifyDataSetChanged();
                return true;

            case R.id.menu_sort_det_desc:
                AppData.getManager().sortConfigs(ConfigOrder.DETERGENT_DESC);
                configAdapter.notifyDataSetChanged();
                return true;

            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        this.selectionTracker.onSaveInstanceState(outState);
        outState.putSerializable("State", this.state);
    }



    private void showAddItemDialog() {
        FragmentManager fragmentManager = getChildFragmentManager();
        DialogAddConfiguration dialog = new DialogAddConfiguration();
        dialog.show(fragmentManager, "AddItemDialog");
    }
}
